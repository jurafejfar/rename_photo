# rename_photo

rename photo according to exif information

# usage

```bash
jura@jevere:~/Pictures$ python3 rename_photo.py -h
usage: rename_photo.py [-h] [-r] [-s] -p PATH

Utility for photos management.

optional arguments:
  -h, --help            show this help message and exit
  -r, --rename          rename files acording to exif
  -s, --sync            remove not pairwise nef files
  -p PATH, --path PATH  path of dir with photos
```

```bash
jura@jevere:~/Pictures$ python3 rename_photo.py -r -p import/
renaming in /home/jura/Pictures/import
    1 directories scanned
   45 image files updated
    1 directories scanned
    0 image files read
```
