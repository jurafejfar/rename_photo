#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import argparse
import subprocess

def rename_file(path):
    dirname = os.path.abspath(path)
    print('renaming in %s' % dirname)
    #subprocess.call(['exiftool', '-r', '-FileName<CreateDate', '-d', 'DSC_%Y%m%d_%H%M%S%%+.c.%%le', dirname])
    #exiftool -CreateDate+='0:0:0 2:54:0' -DateTimeOriginal+='0:0:0 2:54:0' new
    subprocess.call(['exiftool', '-FileName<${CreateDate}_$filenumber.nef', '-d', 'DSC_%Y%m%d_%H%M%S', '-ext', 'nef', dirname])
    subprocess.call(['exiftool', '-FileName<${CreateDate}_$filenumber.jpg', '-d', 'DSC_%Y%m%d_%H%M%S', '-ext', 'JPG', dirname])

def sync_file(path):
    dirname = os.path.abspath(path)
    print('syncing in %s' % dirname)
    for root, dirs, files in os.walk(dirname):
        for fname in files:
            if fname[-3:] == 'nef':
                jpegfname = os.path.splitext(fname)[0]+'.jpg'
                if not os.path.isfile(os.path.join(root, jpegfname)):
                    print('jpeg file %s not exists, removing %s' % (jpegfname, os.path.join(root, fname)))
                    os.remove(os.path.join(root, fname))

def main():
    parser = argparse.ArgumentParser(description="Utility for photos management.")
    parser.add_argument('-r', '--rename', action='store_true', required=False, help='rename files acording to exif')
    parser.add_argument('-s', '--sync', action='store_true', required=False, help='remove not pairwise nef files')
    parser.add_argument('-p', '--path', required=True, help='path of dir with photos')

    args = parser.parse_args()

    if args.rename:
        rename_file(args.path)
    
    if args.sync:
        sync_file(args.path)

if __name__ == '__main__':
    main()
